package edu.kit.informatik.prog.tut;

import java.util.List;
import java.util.Stack;

import final1.algorithm.Game;
import final1.algorithm.GameState;

public class FourWinsGame implements Game<FourWinsMove> {
	
	public enum PlayerCount {
		
		Zero,One,Two;
	}
	
	private PlayerCount playerCount;
	private Stack<Field> gameField;
	
	private Player playerOne;
	private Player playerTwo;
	
	private Player activePlayer;
	
	
	public FourWinsGame(PlayerCount playerCount) {
		
		this.playerCount = playerCount;
		gameField = new Stack<Field>();
		
		initPlayers();
		
		activePlayer = playerOne;
	}
	
	private void initPlayers() {
		
		switch (this.playerCount) {
		
		case Zero:
			playerOne = new AIPlayer(playerTwo, AIPlayer.Level.Easy);
			playerTwo = new AIPlayer(playerOne, AIPlayer.Level.Easy);
			break;
			
		case One:
			playerOne = new HumanPlayer(playerTwo);
			playerTwo = new AIPlayer(playerOne, AIPlayer.Level.Easy);
			break;
			
		case Two:
			playerOne = new HumanPlayer(playerTwo);
			playerTwo = new HumanPlayer(playerOne);
			break;
			
		}
	}

	@Override
	public List<FourWinsMove> getValidMoves() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double evaluateState() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void perform(FourWinsMove move) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void undo() {
		gameField.pop();
	}

	@Override
	public FourWinsMove nextTurn() {
		activePlayer.move();
		activePlayer = activePlayer.next();
	}

	@Override
	public GameState getState() {
		// TODO Auto-generated method stub
		return null;
	}

}
