package edu.kit.informatik.prog.tut;

import final1.algorithm.Game;
import final1.algorithm.GameState;

public class ConnectFour {

	public static void main(String[] args) {
		
		int playerCount = Integer.parseInt(args[0]);
		
		Game<Move> game = new FourWinsGame(playerCount);

		while(game.getState == GameState.PLAYING) {
			
			game.nextTurn();
			
		}

	}

}
