package edu.kit.informatik.prog.tut;

public class AIPlayer implements Player {
	
	public enum Level {
		Easy,Middle,Hard;
	}
	
	private Level level;
	private Player nextPlayer;
	
	public AIPlayer(Player nextPlayer, Level level) {
		
		this.level = level;
		this.nextPlayer = nextPlayer;
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub

	}

	@Override
	public Player next() {
		return nextPlayer;
	}

}
