package edu.kit.informatik.prog.tut;

public class HumanPlayer implements Player {
	
	private Player nextPlayer;
	
	HumanPlayer(Player nextPlayer) {
		
		this.nextPlayer = nextPlayer;
		
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub

	}

	@Override
	public Player next() {
		return nextPlayer;
	}

}
