package edu.kit.informatik.prog.tut;

public class Field {
	
	private int[][] gameField;
	
	private int height;
	private int width;
	
	
	public Field(int width, int height) {
		
		this.height = height;
		this.width = width;
		
		gameField = new int[this.height][this.width];
				
	}
	
	public boolean put(int player, int col) {
		
		if (isFullCol(col)) {
			return false;
		}
		
		int row = 0;
		
		while (gameField[col][row] == 0) {
			row++;
		}
		
		gameField[col][row - 1] = player;
		
		return true;
		
	}
	
	private boolean isFullCol(int col) {
		
		if (gameField[0][col] != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				
				sb.append('|');
				
				switch(gameField[row][col]) {
				
				case 0:
					sb.append(' ');
					break;
				case 1:
					sb.append('1');
					break;
				case 2:
					sb.append('2');
					break;
				
				}
				
			}
			sb.append('|');
		}
		return sb.toString();
		
	}
	
	
}
