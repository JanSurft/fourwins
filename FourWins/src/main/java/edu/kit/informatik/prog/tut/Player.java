package edu.kit.informatik.prog.tut;

public interface Player {
	
	void move();
	
	Player next();

}
